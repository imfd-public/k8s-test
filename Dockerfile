FROM python:3.8-slim-buster

# Ensures our console output looks familiar and is not buffered by Docker, which we don’t want.
ENV PYTHONUNBUFFERED 1
# Will not get prompted for input
ARG DEBIAN_FRONTEND=noninteractive

# Update repos and upgrade packages
RUN apt-get update -qq
RUN apt-get upgrade -yq

# Install apt-utils so debian will not complain about delaying configurations
RUN apt-get install -yq --no-install-recommends apt-utils

# Required for building psycopg2
RUN apt-get install -yq gcc python3-dev curl

# Install poetry
RUN pip install poetry

# Create directory for files
RUN mkdir -p /fastapi/files

#Set working directory
WORKDIR /app

# Create non root user and all folders in which he'll have to write
# RUN adduser --system --group fastapi
# RUN mkdir -p /fastapi/logs /fastapi/media /fastapi/static /fastapi/run /fastapi/locale
# RUN touch /fastapi/logs/fastapi.log
# RUN chown fastapi:fastapi /fastapi/logs/fastapi.log
# RUN chown -R fastapi /fastapi

# Copy the app and do everything app-related
COPY . /app

RUN poetry config virtualenvs.create false
RUN poetry install

# Expose Uvicorn Port
EXPOSE 8000

# Run the entrypoint. This will run gunicorn inside supervisord.
# Note that in dev it runs as sudo, and in prod runs as django.
ENTRYPOINT [ "./scripts/docker-entrypoint.sh" ]
