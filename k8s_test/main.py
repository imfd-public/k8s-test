from typing import Optional
from fastapi import FastAPI, Response, Cookie, Request
from fastapi.middleware.cors import CORSMiddleware
from .settings import settings
import json_logging, logging, sys

logger = logging.getLogger(__name__)

app = FastAPI()
origins = [
    "https://testkor.tk",
    "https://www.testkor.tk",
    "https://objective-goodall-d61641.netlify.app",
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup_event():
    json_logging.init_non_web(enable_json=True)
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.info('hola')


@app.get('/')
def root(response: Response, request: Request, fakesession: Optional[str] = Cookie(None) ):
    logger.info('asking for root')
    response.set_cookie(
        key='fakesession',
        value='my-value',
        httponly=True,
        samesite='strict',
        domain="testkor.tk",
    )

    logger.info(request.headers)
    return {
        'status': 'ok',
        'debug': settings.debug,
        'session_cookie': fakesession,
        'version': 'from gitlab',
    }


@app.get('/delete')
def delete(response: Response):
    logger.error('deleting a cookie')
    response.delete_cookie('fakesession')

    return "deleted"


@app.get('/fail')
def fail():
    raise ValueError('failed')
