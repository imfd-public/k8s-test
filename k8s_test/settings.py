from pydantic import BaseSettings
from typing import Optional


class Settings(BaseSettings):
    debug: Optional[bool]


settings = Settings()
